package com.example.myapplication

import android.widget.ImageButton
import android.widget.ImageView

data class Carta(val id : Int, var resId : Int, var girada : Boolean = false, var igual : Boolean = false)