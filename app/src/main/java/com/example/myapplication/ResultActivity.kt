package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultActivity : AppCompatActivity() {
    lateinit var returnMenu : Button
    lateinit var returngame : Button
    lateinit var sharebutton : Button
    lateinit var punts : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.result_screen)

        returnMenu = findViewById(R.id.return_menu_button)
        returngame = findViewById(R.id.restart_game_buttin)
        sharebutton = findViewById(R.id.share)
        punts = findViewById(R.id.punts)

        val puntiacion = intent.extras!!.getString("puntuacion")
        val valor = intent.extras!!.getString("dificultat")
        punts.setText(puntiacion)

        returngame.setOnClickListener{
            if (valor == "Normal") {
                val intent = Intent(this, GameActivityNormal::class.java)
                intent.putExtra("dificultat",valor)
                startActivity(intent)
            }else if (valor == "Dificil"){
                val intent = Intent(this, GameActivityHard::class.java)
                intent.putExtra("dificultat",valor)
                startActivity(intent)
            }else if (valor == "Hardcore"){
                val intent = Intent(this, GameActivityHardcore::class.java)
                intent.putExtra("dificultat",valor)
                startActivity(intent)
            }
        }

        returnMenu.setOnClickListener{
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }

        sharebutton.setOnClickListener {
           val intent = Intent(android.content.Intent.ACTION_SEND)
           intent.setType("text/plain")
           val mensaje = "Buenas en el juego del Memory_Tarot he sacado una puntuacion de "+ puntiacion + " en el mode " + valor
           intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Puntuacion")
           intent.putExtra(android.content.Intent.EXTRA_TEXT, mensaje)
           startActivity(Intent.createChooser(intent,"Compartir via"))
        }
    }
}