package com.example.myapplication



import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MenuActivity : AppCompatActivity() {

    lateinit var selectordificultat : Spinner
    lateinit var StartButton : Button
    lateinit var HelpButton : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.menu_screen)

        selectordificultat = findViewById(R.id.selectordificultat)
        StartButton = findViewById(R.id.beginbutton)
        HelpButton = findViewById(R.id.buttonhelp)

        ArrayAdapter.createFromResource(this, R.array.dificultat,android.R.layout.simple_spinner_item).also{
                adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            selectordificultat.adapter = adapter}


        StartButton.setOnClickListener{
            val text = selectordificultat.selectedItem.toString()
            if (text == "Normal") {
                val intent = Intent(this, GameActivityNormal::class.java)
                intent.putExtra("dificultat",text)
                startActivity(intent)
            }else if (text == "Dificil"){
                val intent = Intent(this, GameActivityHard::class.java)
                intent.putExtra("dificultat",text)
                startActivity(intent)
            }else if (text == "Hardcore"){
                val intent = Intent(this, GameActivityHardcore::class.java)
                intent.putExtra("dificultat",text)
                startActivity(intent)
            }
        }
        HelpButton.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.title_help))
                .setMessage(resources.getString(R.string.text_help))
                .setNegativeButton(resources.getString(R.string.cierrame_button)) { dialog, which ->
                    closeContextMenu()
                }
                .show()
        }
    }
}