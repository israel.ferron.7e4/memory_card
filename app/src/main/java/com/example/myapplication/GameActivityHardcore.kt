package com.example.myapplication


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class GameActivityHardcore : AppCompatActivity(), View.OnClickListener {

    private lateinit var carta1: ImageButton
    private lateinit var carta2: ImageButton
    private lateinit var carta3: ImageButton
    private lateinit var carta4: ImageButton
    private lateinit var carta5: ImageButton
    private lateinit var carta6: ImageButton
    private lateinit var carta7: ImageButton
    private lateinit var carta8: ImageButton
    private lateinit var carta9: ImageButton
    private lateinit var resetBut: Button
    private lateinit var numMov: TextView
    private lateinit var stopBut: ImageView

    private lateinit var buttons: Array<ImageButton>
    var puntuacionMax = 1800

    private lateinit var viewModel: GameViewModelHarcore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.game_screen_hardcore)

        viewModel = ViewModelProvider(this).get(GameViewModelHarcore::class.java)

        carta1 = findViewById(R.id.carta_1)
        carta2 = findViewById(R.id.carta_2)
        carta3 = findViewById(R.id.carta_3)
        carta4 = findViewById(R.id.carta_4)
        carta5 = findViewById(R.id.carta_5)
        carta6 = findViewById(R.id.carta_6)
        carta7 = findViewById(R.id.carta_7)
        carta8 = findViewById(R.id.carta_8)
        carta9 = findViewById(R.id.carta_9)
        numMov = findViewById(R.id.numMov)
        resetBut = findViewById(R.id.buttonRest)
        stopBut = findViewById(R.id.buttonStop)



        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)
        carta7.setOnClickListener(this)
        carta8.setOnClickListener(this)
        carta9.setOnClickListener(this)

        buttons = arrayOf(carta1, carta2, carta3, carta4, carta5, carta6, carta7, carta8, carta9);


        resetBut.setOnClickListener {
            reset()
            updata()
        }

        stopBut.setOnClickListener {
            stop()
        }
        numMov.setText("Moviments: 0")

        updata()
    }

    private fun girarCarta(idCarta: Int, carta: ImageButton) {
        numMov.setText("Moviments: " + (viewModel.moviments + 1))
        carta.setImageResource(viewModel.comprovar(idCarta))
        var primerMatch: Carta? = null
        var segonMatch: Carta? = null
        var tercerMatch: Carta? = null
        for (carta in viewModel.cartes) {
            if (carta.girada && !carta.igual) {
                if (primerMatch == null) {
                    primerMatch = carta
                    buttons[carta.id].setOnClickListener(null)
                } else if (primerMatch != null && segonMatch == null) {
                    segonMatch = carta
                    buttons[carta.id].setOnClickListener(null)
                } else {
                    tercerMatch = carta
                    buttons[carta.id].setOnClickListener(null)
                }
            }
        }
        Handler(Looper.getMainLooper()).postDelayed({
            matchCarta(primerMatch, segonMatch, tercerMatch)
        }, 450)
        viewModel.moviments++
    }


    fun matchCarta(primerMatch: Carta?, segonMatch: Carta?, tercerMatch: Carta?) {
        if (primerMatch != null && segonMatch != null && tercerMatch != null) {
            if (primerMatch.resId != segonMatch.resId && primerMatch.resId != tercerMatch.resId && segonMatch.resId != tercerMatch.resId) {
                buttons[primerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[segonMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[tercerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[primerMatch.id].setOnClickListener(this)
                buttons[segonMatch.id].setOnClickListener(this)
                buttons[tercerMatch.id].setOnClickListener(this)
                primerMatch.girada = false
                segonMatch.girada = false
                tercerMatch.girada = false
                viewModel.fallostrio++
            } else if (primerMatch.resId == segonMatch.resId && primerMatch.resId != tercerMatch.resId && segonMatch.resId != tercerMatch.resId) {
                buttons[primerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[segonMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[tercerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[primerMatch.id].setOnClickListener(this)
                buttons[segonMatch.id].setOnClickListener(this)
                buttons[tercerMatch.id].setOnClickListener(this)
                primerMatch.girada = false
                segonMatch.girada = false
                tercerMatch.girada = false
                viewModel.fallospar++
            } else if (primerMatch.resId != segonMatch.resId && primerMatch.resId == tercerMatch.resId && segonMatch.resId != tercerMatch.resId) {
                buttons[primerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[segonMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[tercerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[primerMatch.id].setOnClickListener(this)
                buttons[segonMatch.id].setOnClickListener(this)
                buttons[tercerMatch.id].setOnClickListener(this)
                primerMatch.girada = false
                segonMatch.girada = false
                tercerMatch.girada = false
                viewModel.fallospar++
            } else if (primerMatch.resId != segonMatch.resId && primerMatch.resId != tercerMatch.resId && segonMatch.resId == tercerMatch.resId) {
                buttons[primerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[segonMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[tercerMatch.id].setImageResource(R.drawable.reverso_carta)
                buttons[primerMatch.id].setOnClickListener(this)
                buttons[segonMatch.id].setOnClickListener(this)
                buttons[tercerMatch.id].setOnClickListener(this)
                primerMatch.girada = false
                segonMatch.girada = false
                tercerMatch.girada = false
                viewModel.fallospar++
            } else if (primerMatch.resId == segonMatch.resId && segonMatch.resId == tercerMatch.resId) {
                primerMatch.igual = true
                buttons[primerMatch.id].setOnClickListener(null)
                segonMatch.igual = true
                buttons[segonMatch.id].setOnClickListener(null)
                tercerMatch.igual = true
                buttons[tercerMatch.id].setOnClickListener(null)
            }
        }
        if (viewModel.finalGame()) {
            val valor = intent.extras!!.getString("dificultat")
            val points = puntuacionMax - (viewModel.fallospar * 20 + viewModel.fallostrio * 50)
            val intent = Intent(this, ResultActivity::class.java)
            intent.putExtra("dificultat", valor)
            intent.putExtra("puntuacion", points.toString())
            startActivity(intent)
        }
    }

    private fun reset() {
        viewModel.moviments = 0
        numMov.setText("Moviments: " + viewModel.moviments)
        for (button in buttons) {
            button.setOnClickListener(this)
            button.setImageResource(R.drawable.reverso_carta)
        }
        for (carta in viewModel.cartes) {
            carta.igual = false
            carta.girada = false
        }
    }

    private fun stop() {
        if (viewModel.parado == 0) {
            numMov.setText("El joc esta en pausa")
            for (button in buttons) {
                button.setOnClickListener(null)
            }
            resetBut.setOnClickListener(null)
            viewModel.parado = 1
        } else if (viewModel.parado == 1) {
            numMov.setText("Moviments: " + viewModel.moviments.toString())
            for (button in buttons) {
                button.setOnClickListener(this)
            }
            resetBut.setOnClickListener {
                reset()
                updata()
            }
            viewModel.parado = 0
        }
    }


    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
            carta7 -> girarCarta(6, carta7)
            carta8 -> girarCarta(7, carta8)
            carta9 -> girarCarta(8, carta9)
        }
    }

    private fun updata() {
        carta1.setImageResource(viewModel.statCarta(0))
        carta2.setImageResource(viewModel.statCarta(1))
        carta3.setImageResource(viewModel.statCarta(2))
        carta4.setImageResource(viewModel.statCarta(3))
        carta5.setImageResource(viewModel.statCarta(4))
        carta6.setImageResource(viewModel.statCarta(5))
        carta7.setImageResource(viewModel.statCarta(6))
        carta8.setImageResource(viewModel.statCarta(7))
        carta9.setImageResource(viewModel.statCarta(8))
        if (viewModel.parado == 1) {
            numMov.setText("El joc esta en pausa")
            for (button in buttons) {
                button.setOnClickListener(null)
            }
        } else if (viewModel.parado == 0) {
            numMov.setText("Moviments: " + viewModel.moviments.toString())
            for (button in buttons) {
                button.setOnClickListener(this)
            }
        }
    }
}
