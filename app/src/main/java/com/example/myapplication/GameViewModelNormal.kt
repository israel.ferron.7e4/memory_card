package com.example.myapplication

import androidx.lifecycle.ViewModel

class GameViewModelNormal: ViewModel(){
    var moviments = 0
    var parado = 0
    var fallos = 0
    var imatges = arrayOf(
        R.drawable.carta_1,
        R.drawable.carta_1,
        R.drawable.carta_2,
        R.drawable.carta_2,
        R.drawable.carta_3,
        R.drawable.carta_3,
    )
    var cartes = mutableListOf<Carta>()


    init {
        setDataModal()
    }

    private fun setDataModal(){
        imatges.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i,imatges[i]))
        }
    }

    fun comprovar(id : Int) : Int{
        if (!cartes[id].girada){
            cartes[id].girada = true
            return cartes[id].resId
        }else{
            cartes[id].girada = false
            return R.drawable.reverso_carta
        }

    }

    fun finalGame() : Boolean{
        for (carta in cartes){
            if (!carta.igual){
                return false
            }
        }
        return true
    }

    fun statCarta(idCarta :Int) :Int{
        if (cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.reverso_carta
    }

}
